package com.godohembon.customannotation

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import com.godohembon.customannotation.databinding.ActivityMainBinding
import com.godohembon.processor_example.UsedInReflection
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding

    @UsedInReflection("com.godohembon.customannotation.FirstFragment")
    var firstFragmentReflection: String = GeneratedReflectionMainActivity.firstFragmentReflection

    @UsedInReflection("com.godohembon.customannotation.SecondFragment")
    var secondFragmentReflection: String = GeneratedReflectionMainActivity.secondFragmentReflection

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Toast.makeText(this, firstFragmentReflection, Toast.LENGTH_SHORT).show()
        setSupportActionBar(binding.toolbar)

        val navController = findNavController(R.id.nav_host_fragment_content_main)
        appBarConfiguration = AppBarConfiguration(navController.graph)
        setupActionBarWithNavController(navController, appBarConfiguration)

        binding.fab.setOnClickListener { view ->
            Snackbar.make(view, firstFragmentReflection, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
    }
}