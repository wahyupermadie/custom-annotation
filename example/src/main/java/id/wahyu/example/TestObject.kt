package id.wahyu.example

import com.godohembon.processor_example.UsedInReflection

object TestObject {

    @UsedInReflection("com.godohembon.customannotation.FirstFragment")
    var testObjectReflection = GeneratedReflectionTestObject.testObjectReflection
}