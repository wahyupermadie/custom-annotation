package com.godohembon.processor_example

@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
annotation class UsedInReflection(
    val value: String
)
