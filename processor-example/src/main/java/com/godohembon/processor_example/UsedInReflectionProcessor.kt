package com.godohembon.processor_example

import com.google.auto.service.AutoService
import com.squareup.kotlinpoet.*
import com.sun.beans.finder.ClassFinder.findClass
import jdk.nashorn.internal.objects.annotations.Property
import java.io.File
import java.util.*
import javax.annotation.processing.AbstractProcessor
import javax.annotation.processing.Processor
import javax.annotation.processing.RoundEnvironment
import javax.lang.model.SourceVersion
import javax.lang.model.element.Element
import javax.lang.model.element.ElementKind
import javax.lang.model.element.TypeElement

@AutoService(Processor::class)
class UsedInReflectionProcessor: AbstractProcessor() {
    override fun getSupportedSourceVersion(): SourceVersion {
        return SourceVersion.latestSupported()
    }

    override fun getSupportedAnnotationTypes(): MutableSet<String> {
        return mutableSetOf(UsedInReflection::class.java.name)
    }

    override fun process(
        annotations: MutableSet<out TypeElement>?,
        roundEnv: RoundEnvironment?
    ): Boolean {
        return processAnnotations(annotations, roundEnv)
    }


    private fun processAnnotations(
        annotations: MutableSet<out TypeElement>?,
        roundEnv: RoundEnvironment?
    ): Boolean {
        try {
            if (roundEnv == null) {
                processingEnv.noteMessage { "RoundEnvironment is null hence skip process." }
                return true
            }

            if (annotations == null || annotations.isEmpty()) {
                processingEnv.noteMessage { "TypeElements is null or empty hence skip process." }
                return true
            }

            val elements = roundEnv.getElementsAnnotatedWith(UsedInReflection::class.java)
            if (elements.isEmpty()) {
                processingEnv.noteMessage { "Not able to find ${UsedInReflection::class.java.name} in RoundEnvironment" }
                return true
            }

            val generatedSource = processingEnv.options[KAPT_KOTLIN_GENERATED] ?: run {
                processingEnv.errorMessage { "Can't find target source." }
                return true
            }

            var fileName    = "GeneratedReflection"
            var objectBuilder = TypeSpec.objectBuilder("")
            var oldEnclosingElement = ""
            val iterator = elements.iterator()
            while (iterator.hasNext()) {
                val element = iterator.next()
                if (element.kind != ElementKind.FIELD) {
                    processingEnv.errorMessage { "This annotation only aplied for ${element.kind} type" }
                    return true
                }

                val annotation = element.getAnnotation(UsedInReflection::class.java)

                try {
                    Class.forName(annotation.value)
//                    val typeElement = processingEnv.elementUtils
//                        .getTypeElement(annotation.value).asType()
                    processingEnv.noteMessage {
                        "Reflection class found for ${annotation.value}"
                    }
                } catch (e: ClassNotFoundException) {
                    e.printStackTrace()
                }

                val propertyBuilder = PropertySpec.builder(
                    name = "$element",
                    type = ClassName("kotlin","String"),
                    modifiers = arrayOf(KModifier.CONST, KModifier.FINAL)
                ).mutable(false).initializer("\"${annotation.value}\"")

                val newEnclosingElement = element.enclosingElement.toString()
                val className = newEnclosingElement.split(".").lastOrNull()

                if (oldEnclosingElement.isEmpty()) {
                    val packageName = newEnclosingElement
                        .replace(
                            ".${className}", " "
                        )
                        .toLowerCase()
                        .trim()
                    oldEnclosingElement = newEnclosingElement
                    fileName = "GeneratedReflection$className"
                    objectBuilder = TypeSpec.objectBuilder(fileName)
                    objectBuilder.addProperty(propertyBuilder.build())
                    if (!iterator.hasNext()) {
                        generateClass(generatedSource, packageName, fileName, objectBuilder)
                    }
                } else {
                    val packageName = newEnclosingElement
                        .replace(
                            ".${className}", " "
                        ).toLowerCase()
                        .trim()

                    if (oldEnclosingElement != newEnclosingElement) {
                        generateClass(generatedSource, packageName, fileName, objectBuilder)
                        fileName = "GeneratedReflection$className"
                        objectBuilder = TypeSpec.objectBuilder(fileName)
                        objectBuilder.addProperty(propertyBuilder.build())
                        if (!iterator.hasNext()) {
                            generateClass(generatedSource, packageName, fileName, objectBuilder)
                        }
                        oldEnclosingElement = newEnclosingElement
                    } else {
                        objectBuilder.addProperty(propertyBuilder.build())
                        if (!iterator.hasNext()) {
                            generateClass(generatedSource, packageName, fileName, objectBuilder)
                        }
                    }
                }
            }

        }catch (e: Exception) {
            processingEnv.errorMessage {
                "error "+e.message
            }
        }

        return true
    }

    private fun generateClass(
        generatedSource: String,
        packageName: String,
        fileName: String,
        objectBuilder: TypeSpec.Builder
    ){
        val file = FileSpec.builder(packageName, fileName)
            .addType(objectBuilder.build())
            .build()
        file.writeTo(File(generatedSource))
    }

    companion object {
        const val KAPT_KOTLIN_GENERATED = "kapt.kotlin.generated"
    }
}